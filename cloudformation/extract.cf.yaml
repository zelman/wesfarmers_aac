AWSTemplateFormatVersion: "2010-09-09"
Description: VPC for project
Parameters:
  Environment:
    Description: An environment name that will be prefixed to resource names
    Type: String
  VpcCIDR:
    Description: Please enter the IP range (CIDR notation) for this VPC
    Type: String
    Default: 10.192.0.0/16
  PublicSubnet1CIDR:
    Description: Please enter the IP range (CIDR notation) for the public subnet in the first Availability Zone
    Type: String
    Default: 10.192.10.0/24
  KeyName:
    Description: Name of the KeyPair for connecting to the EC2 instance
    Type: String
    Default: "wesfarmers"
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Ref Environment
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
        - Key: Name
          Value: !Ref Environment
  InternetGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC
  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !Ref PublicSubnet1CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub ${Environment} Public Subnet (AZ1)
  NatGateway1EIP:
    Type: AWS::EC2::EIP
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc
  NatGateway1:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt NatGateway1EIP.AllocationId
      SubnetId: !Ref PublicSubnet1
  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub ${Environment} Public Routes
  DefaultPublicRoute:
    Type: AWS::EC2::Route
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet1
  EC2SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: "ec2-sg"
      GroupDescription: "Security group for EC2 instance to connect to internet"
      SecurityGroupIngress:
         - IpProtocol: tcp
           CidrIp: 0.0.0.0/0
           FromPort: 22
           ToPort: 22
      VpcId: !Ref VPC
  
  EC2Instance:
    Type: AWS::EC2::Instance
    DependsOn:
      - EC2InstanceProfile
      - EC2SecurityGroup
      - PublicSubnet1
    Properties: 
      ImageId: "ami-06202e06492f46177"
      IamInstanceProfile: !Ref EC2InstanceProfile
      InstanceType: "t2.micro"
      KeyName: !Ref KeyName
      SecurityGroupIds: 
        - !Ref EC2SecurityGroup
      SubnetId: !Ref PublicSubnet1
#      UserData: String
      # Volumes: 
      #   - Volume
  EC2ExecutionRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service: ec2.amazonaws.com
            Action:
              - "sts:AssumeRole"
      Description: "IAM Role for the EC2 instance"
      Policies:
        - PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: "Allow"
                Action: 
                  - "s3:PutObject"
                  - "s3:GetObject"
                Resource:
                  - !Join
                    - "/"
                    - - !ImportValue S3Bucket
                      - "*"
              - Effect: "Allow"
                Action: "s3:ListBucket"
                Resource: 
                  - !ImportValue S3Bucket
          PolicyName: !Sub "ec2-execution-policy"
      Path: "/"
      RoleName: "ec2-execution-role"
  EC2InstanceProfile:
    Type: "AWS::IAM::InstanceProfile"
    DependsOn:
      - EC2ExecutionRole
    Properties:
      InstanceProfileName: "EC2InstanceProfile"
      Roles:
        - !Ref EC2ExecutionRole
Outputs:
  VPC:
    Description: A reference to the created VPC
    Value: !Ref VPC
    Export:
      Name: VPC
  PublicSubnets:
    Description: A list of the public subnets
    Value: !Join [ ",", [ !Ref PublicSubnet1]]
  PublicSubnet1:
    Description: A reference to the public subnet in the 1st Availability Zone
    Value: !Ref PublicSubnet1
    Export:
      Name: PublicSubnet1
  EC2SecurityGroup:
    Description: Security group for EC2 instances
    Value: !Ref EC2SecurityGroup
    Export:
      Name: EC2SecurityGroup
