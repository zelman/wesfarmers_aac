/* This can take 20+ minutes to run*/
MSCK REPAIR TABLE bom_weather_reads;



select location, year, count(max_temp) num_days 
from 
(
select col0 as station, substr(col1,-4) as year, col5 as max_temp
from vic
where col0 in ('BREAKWATER (GEELONG RACECOURSE)','SCORESBY RESEARCH INSTITUTE') 
  and date_parse(col1,'%d/%m/%Y') >= date_parse('01/01/2012','%d/%m/%Y')
  union all
  select col0 as station, substr(col1,-4) as year, col5 as max_temp
from wa
where col0 = 'PERTH AIRPORT'
  and date_parse(col1,'%d/%m/%Y') >= date_parse('01/01/2012','%d/%m/%Y')
) subq

join bom_mappings on subq.station = bom_mappings.station 
where max_temp > 35
group by location, year
order by location, year