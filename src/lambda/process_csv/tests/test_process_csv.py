import pytest
import process_csv as src

context = {}
event = {'Records': [{'messageId': 'bab75033-872c-4a06-9e58-c0ec3d343f86', 'receiptHandle': 'AQEB6WTlsyQXoqerPRm', 'body': '{"Records":[{"eventVersion":"2.1","eventSource":"aws:s3","awsRegion":"ap-southeast-2","eventTime":"2021-04-05T05:05:05.895Z","eventName":"ObjectCreated:Put","userIdentity":{"principalId":"AWS:AROAZFA6KYZEZWUVQRZWS:i-08bc33d021898f0cd"},"requestParameters":{"sourceIPAddress":"3.26.71.127"},"responseElements":{"x-amz-request-id":"9ESKHDRVTH9JQYTK","x-amz-id-2":"nCRW6F8/xWldQN2muyG/JpsgOk0Sva2MYrUA2o63tU3MlaQ+F1Tiv7NRQl2hLaHiVn7KVQmsa92qJUr6tsqS54Tp9ENXYpSI"},"s3":{"s3SchemaVersion":"1.0","configurationId":"2e401859-4860-4bda-aa06-c7849847cecb","bucket":{"name":"bom-dev-landing","ownerIdentity":{"principalId":"A14O737T0GM308"},"arn":"arn:aws:s3:::bom-dev-landing"},"object":{"key":"tables/sa/minnipa_pirsa/minnipa_pirsa-202003.csv","size":2678,"eTag":"ac614efee825f36f42a7c47fafc657bd","sequencer":"00606A9A8949F4EF5B"}}}]}', 'attributes': {'ApproximateReceiveCount': '1', 'SentTimestamp': '1617599114607', 'SenderId': 'AIDAI32VHCC23ON2HJ2FY', 'ApproximateFirstReceiveTimestamp': '1617599114609'}, 'messageAttributes': {}, 'md5OfBody': 'f7922a650f8eae8110f45a29d6a767f8', 'eventSource': 'aws:sqs', 'eventSourceARN': 'arn:aws:sqs:ap-southeast-2:629276329545:bom_readings', 'awsRegion': 'ap-southeast-2'}]}


def test_happy_path():
    # Patch out the calls to S3
    # result = src.lambda_handler(event,context)
    
    # assert s3 Get Object with the right details from the event
    # assert s3 Put Object with the right details from the event
    
    assert True # only do this for the sake of the case study.  Normally assert false until test is written

def test_invalid_rows():
    # Patch out the calls to S3, dummy file with non-utf8 characters, and rows that don't start with the expected string
    # result = src.lambda_handler(event,context)
    
    # assert no errors
    # assert s3 Put Object did not occur

    assert True # only do this for the sake of the case study.  Normally assert false until test is written

def test_mixed_rows():
    # Patch out the calls to S3, dummy file with some valid rows, some invalid
    # result = src.lambda_handler(event,context)
    
    # assert no errors
    # assert s3 Put Object did occur, and payload matches expected

    assert True # only do this for the sake of the case study.  Normally assert false until test is written
    
