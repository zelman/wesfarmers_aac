#!/bin/sh
 
set -e
 
if [ -z "$1" ]
then
  echo "ERROR: Provide a function name as a parameter"
  exit 1
fi
 
mkdir zip_folder
cp *.py  zip_folder
 
cd zip_folder
 
pip3 install --target . pytz


zip -r9 function.zip .
 
aws lambda update-function-code --function-name $1 --zip-file fileb://function.zip --publish
 
cd ..
 
rm -r zip_folder
