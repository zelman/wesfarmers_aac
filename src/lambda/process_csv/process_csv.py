import json
import urllib.parse
import boto3
import csv

s3 = boto3.client('s3')


def lambda_handler(event, context):
    main(event)
    
def main(event):
    for msg in event['Records']:
        records = json.loads(msg['body'])
        for rec in records['Records']:
            # Get the object from the event
            bucket = rec['s3']['bucket']['name']
            key = urllib.parse.unquote_plus(rec['s3']['object']['key'], encoding='utf-8')
            response = s3.get_object(Bucket=bucket, Key=key)
        
            # Get details from the key
            key_parts = key.split("/")
            location = key_parts[2]
            loc_formatted = location.replace("_"," ")
            filename = key_parts[-1]
            year = filename[-10:-6] # assume filename always xxxxx-yyyymm.csv
            state = key_parts[1]
            
        
            # read the lines from the file
            lines = response['Body'].iter_lines()
            output_list= []
            num_processed=0
            for row in lines:
                num_processed += 1
                # there are rows that can't be converted to UTF-8, use try/except
                try:
                    row_string = row.decode('utf-8')
                    # only want the data rows, which can be determined by checking that the location is the first field in the line
                    if  row_string.lower().startswith(loc_formatted):
                        output_list.append(row_string)
                except UnicodeDecodeError as e:
                    # expected to get here, just need to move onto the next line
                    pass
            num_valid = len(output_list)
            
            if num_valid > 0:
                # put together the output list back into lines        
                body = '\n'.join(output_list)
                
                output_key = 'cleansed/state={}/station={}/year={}/{}'.format(state, location, year, filename)
                s3.put_object(Bucket=bucket, Body=body.encode(),Key=output_key)
                print('Data uploaded to {}'.format(output_key))
            print('{} rows processed, {} valid'.format(num_processed, num_valid))  
    return
