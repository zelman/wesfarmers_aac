# Wesfarmers AAC Case Study

## Problem Statement
Use the weather details available from the Bureau of Meteorology to determine how many days in the past nine years, each of these Wesfarmers sites experienced temperatures > 35 degrees
- Bunnings Notting Hill, Vic
- Officeworks Geelong, Vic
- Kmart Belmont, WA

# Prerequisites
- Python 3
- Pytest
- Boto3
- AWS Cli configured to your AWS account

# Running the makefile
- **make deploy**: run all the tests, then deploy the solution
- **make destroy**: remove the cloudformation stacks
- **make test**: validate the cloudformation templates, and run pytest