PROCESS_TEMPLATE=process.cf.yaml
CONFIG_FILE=wesfarmers.cfg
CONFIG=dev
PROCESS_STACK_NAME=process-${CONFIG}

EXTRACT_TEMPLATE=extract.cf.yaml
EXTRACT_STACK_NAME=extract-${CONFIG}

validate-templates:
	$(eval TEMPLATES = $(shell find . -name '*.cf.yaml' | sort | uniq))
	@for template in $(TEMPLATES); do \
		echo "--- :cloudformation: Validating $$template" && \
		aws cloudformation validate-template \
			--template-body file://./$$template \
			--output table; \
		if [ $$? -ne 0 ]; then \
			echo "^^^ Validation failed, oh no!!" && \
			exit 1; \
		fi \
	done
	@echo "Validation Successful, good job!!"

unit-tests:
	@echo "run pytest across the code base"
	pytest -v


deploy-extract-stack:
	@if [ -f ./cloudformation/$(EXTRACT_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(EXTRACT_STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(EXTRACT_TEMPLATE) \
			--stack-name $(EXTRACT_STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG)/$(CONFIG_FILE)); \
	else \
		echo --- :sadpanda: No Extract template found.; \
	fi;

deploy-process-stack:
	@if [ -f ./cloudformation/$(PROCESS_TEMPLATE) ] ; then \
		echo "--- :cloudformation: Deploying stack $(PROCESS_STACK_NAME)"; \
		aws cloudformation deploy \
			--no-fail-on-empty-changeset \
			--capabilities CAPABILITY_NAMED_IAM \
			--template ./cloudformation/$(PROCESS_TEMPLATE) \
			--stack-name $(PROCESS_STACK_NAME) \
			--parameter-overrides \
				Configuration=$(CONFIG) \
				$(shell cat ./config/$(CONFIG)/$(CONFIG_FILE)); \
	else \
		echo --- :sadpanda: No Process template found.; \
	fi;

deploy-lambda-code:
	@echo "---run the deploy.sh for each lambda to upload the code"
	cd ./src/lambda/process_csv && ./deploy.sh bom_readings

delete-extract-stack:
	@echo "--- :cloudformation: Deleting extract stack"; \
	aws cloudformation delete-stack --stack-name $(EXTRACT_STACK_NAME)

delete-process-stack:
	@echo "--- :cloudformation: Deleting process stack"; \
	aws cloudformation delete-stack --stack-name $(PROCESS_STACK_NAME)

deploy: test deploy-process-stack deploy-extract-stack deploy-lambda-code
destroy: delete-extract-stack delete-process-stack
test: validate-templates unit-tests
